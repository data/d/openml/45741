# OpenML dataset: beans

https://www.openml.org/d/45741

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset aims to distinguish seven different types of dry beans, taking into account the features such as form, shape, type, and structure by the market situation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45741) of an [OpenML dataset](https://www.openml.org/d/45741). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45741/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45741/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45741/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

